export class Boleta {
    numero = null;
    subtotal = null;
    gananciaFuncionario = null;

    constructor(boleta = null){
        if(boleta){
            this.numero = boleta.numero;
            this.subtotal = boleta.subtotal;
            this.gananciaFuncionario = boleta.gananciaFuncionario;
        }
    }
 
}