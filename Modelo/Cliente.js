export class Cliente {
    nombre = null;
    apellido = null;
    rut = null;
    email = null;
    contrasenhia = null;
    locacionLongitud = null;
    locacionLatitud = null;

    constructor(cliente = null){
        if(cliente){
        this.nombre = cliente.nombre;
        this.apellido = cliente.apellido;
        this.email = cliente.email;
        this.contrasenhia = cliente.contrasenhia;
        this.locacionLatitud = cliente.locacionLatitud;
        this.locacionLongitud = cliente.locacionLongitud;
        }
    }
}