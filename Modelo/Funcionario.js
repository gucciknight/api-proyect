export class Funcionario {
    nombre = null;
    apellido = null;
    rut = null;
    fechaDeNacimiento = null;
    locacionLongitud = null;
    locacionLatitud = null;

    constructor(funcionario = null){
        if(uncionario){
            this.nombre = funcionario.nombre;
            this.apellido = funcionario.apellido;
            this.rut = funcionario.rut;
            this.locacionLatitud = funcionario.locacionLatitud;
            this.locacionLongitud = funcionario.locacionLongitud;
        }
    }
}