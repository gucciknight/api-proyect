export class Pedido {
    id = null;
    fecha = null;
    calidad = null;

    constructor(pedido = null){
        this.id = null;
        this.fecha = pedido.fecha;
        this.calidad = pedido.calidad;
    }
}