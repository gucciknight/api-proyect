export class Producto {
    codigoDeBarras = null;
    nombre = null;
    marca = null;
    precio = null;

    constructor(producto = null) {
        this.codigoDeBarras = producto.codigoDeBarras;
        this.nombre = producto.nombre;
        this.marca = producto.marca;
        this.precio = producto.precio;
    }
}